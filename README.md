# My Linux Configurations and Optional Scripts

## Archlinux Packages

```
base base-devel linux linux-firmware grub os-prober ntfs-3g
gcc make git curl wget sudo vim nano zsh python2 python
xorg xorg-server xorg-xinit xorg-xrandr xorg-drivers
openbox lxappearance lxappearance-obconf i3lock-color
picom nitrogen xcape gnome-keyring
polkit polkit-gnome mesa mesa-demos
poppler-glib ffmpegthumbnailer freetype2 libgsf raw-thumbnailer
gtk-engines gvfs gvfs-smb gvfs-mtp zenity
nemo nemo-fileroller nemo-preview nemo-seahorse nemo-share
vlc qbittorrent clementine leafpad xfce4-terminal xed
firefox gnome-disk-utility htop pavucontrol
libreoffice-still breeze breeze-gtk breeze-icons
pulseaudio pulseaudio-alsa qt5ct lxtask eog eog-plugins
archlinux-xdg-menu xdg-user-dirs gnome-backgrounds 
gnome-themes-extra p7zip unrar ttf-droid ttf-dejavu
networkmanager dhcpcd dnsmasq samba network-manager-applet
wpa_supplicant create_ap hostapd gst-plugins-good
gst-plugins-bad gst-plugins-ugly
```

For AMD GPU - xf86-video-amdgpu
For Intel GPU - xf86-video-intel
For generic support - xf86-video-fbdev xf86-video-vesa

For Panel setup install either of the following set of apps -

```
tint2 parcellite volumeicon cbatticon jgmenu notification-daemon
```

or 

```
xfce4-panel xfce4-goodies
```

While installing `xfce4-goodies` may select individual plugins
manually.

Prepare samba (as root) - 

```
mkdir /var/lib/samba/usershares
groupadd -r sambashare
chown root:sambashare /var/lib/samba/usershares
chmod 1770 /var/lib/samba/usershares
```
Create user -

```
useradd -m -G network,power,sambashare,storage,wheel -s /bin/zsh shimon
```

As normal user show desktop icons and set nemo default terminal to
xfce4 terminal -

```
gsettings set org.nemo.desktop show-desktop-icons true
gsettings set org.cinnamon.desktop.default-applications.terminal exec xfce4-terminal
```

